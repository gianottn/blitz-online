import React from 'react';
import ReactDOM from 'react-dom';

import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';

class Article extends React.Component {
  constructor(props) {
    super(props);

    var year = document.querySelector('#article').dataset.year;
    var years = Object.values(JSON.parse(document.querySelector('#article').dataset.years));
    var issue = document.querySelector('#article').dataset.issue;
    var issues = document.querySelector('#article').dataset.issues;
    var article = document.querySelector('#article').dataset.article;
    var articles = document.querySelector('#article').dataset.articles;

    var issue = (issue == 'none') ? issue : JSON.parse(issue);
    var issues = (issues == 'none') ? issues : JSON.parse(issues);
    var article = (article == 'none') ? article : JSON.parse(article);
    var articles = (articles == 'none') ? articles : JSON.parse(articles);

    this.select = this.select.bind(this);
    this.selectYear = this.selectYear.bind(this);
    this.selectIssue = this.selectIssue.bind(this);
    this.selectArticle = this.selectArticle.bind(this);

    this.handleChangeYear = this.handleChangeYear.bind(this);
    this.handleChangeIssue = this.handleChangeIssue.bind(this);
    this.handleChangeArticle = this.handleChangeArticle.bind(this);

    this.article = this.article.bind(this);

    this.state = {
      year: year,
      years: years,
      issue: issue,
      issues: issues,
      article: article,
      articles: articles,
    };
  }

  handleChangeYear(event) {
    window.location = "/articles/" + event.target.value; 
  }

  handleChangeIssue(event) {
    window.location = "/articles/" + this.state.year + "/" + event.target.value; 
  }

  handleChangeArticle(event) {
    window.location = "/articles/" + this.state.year + "/" + this.state.issue.id + "/" + event.target.value; 
  }

  selectYear() {
    return (
      <Select value={this.state.year} onChange={this.handleChangeYear} style={{ margin: '10px' }} className="articleSelector">
        {
          this.state.years.map(year => (
            <MenuItem selected value={year}>{year}</MenuItem>
          ))
        }
      </Select>
    );
  }

  selectIssue() {
    return (
      <Select value={this.state.issue.id} onChange={this.handleChangeIssue} style={{ margin: '10px'}} className="articleSelector">
        {
          this.state.issues.map(issue => (
            <MenuItem value={issue.id}>{issue.semester}{this.state.year.substr(2,4)} {issue.number}: {issue.title}</MenuItem>
          ))
        }
      </Select>
    );
  }

  selectArticle() {
    return (
      <Select value={this.state.article.id} onChange={this.handleChangeArticle} style={{ margin: '10px'}} className="articleSelector">
        {
          this.state.articles.map(article => (
            <MenuItem value={article.id}>{article.title}</MenuItem>
          ))
        }
      </Select>
    );
  }

  article() {
    return (
      <div style={{ textAlign: 'left' }}>
        <div className="articleTitle">
          <div className="container">
            <h1 style={{ textAlign: 'center', padding: '20px' }}>{this.state.article.title}</h1>
            <p style={{ display: 'flex', justifyContent: 'space-between' }}><span>{this.state.article.author}</span><span>{this.state.article.email}</span></p>
          </div>
        </div>
        <div className="container" style={{ marginTop: '20px' }}>
          <i><div dangerouslySetInnerHTML={{ __html: this.state.article.abstract }}></div></i>
          <div dangerouslySetInnerHTML={{ __html: this.state.article.body }}></div>
          <ol dangerouslySetInnerHTML={{ __html: this.state.article.footnotes }}></ol>
        </div>
      </div>
    );
  }

  select() {
    if(this.state.article != 'none') {
      return(
        <div>
          <div className="selector text-white">
            <div className="container">
              {this.selectYear()}
              {this.selectIssue()}
              {this.selectArticle()}
            </div>
          </div>
          <div>
            {this.article()}
          </div>
        </div>
      );
    }
    if(this.state.issue != 'none') {
      return(
        <div className="selector">
          {this.selectYear()}
          {this.selectIssue()}
          {this.selectArticle()}
        </div>
      );
    } else if(this.state.year != 'none') {
      return (
        <div className="selector">
          {this.selectYear()}
          {this.selectIssue()}
        </div>
      );
    } else {
      return (
        <div className="selector">
          {this.selectYear()}
        </div>
      );
    }
  }

  render() {
    return(
      <div style={{ textAlign: 'center' }}>
        {this.select()}
      </div>
    );
  }
}

ReactDOM.render(<Article/>, document.getElementById('article'));

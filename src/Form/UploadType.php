<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('title', TextType::class, [
              'label' => 'Title',
              'required' => true, 
          ])
          ->add('semester', ChoiceType::class, [
              'label' => 'Semester',
              'required' => true,
              'expanded' => false,
              'multiple' => false, 
              'choices' => [
                  'HS' => 'HS',
                  'FS' => 'FS', 
              ],
          ])
          ->add('issue', IntegerType::class, [
              'label' => 'Issue',
              'required' => true, 
          ])
          ->add('year', IntegerType::class, [
              'label' => 'Year',
              'required' => true, 
              'data' => date('Y'),
          ])
          ->add('html_export', FileType::class, [
              'label' => 'Redsys HTML Export',
              'required' => true,
              'attr' => [
                'accept' => 'text/html', 
              ],
          ])
          ->add('images', FileType::class, [
              'label' => 'All images',
              'required' => false,
              'multiple' => true,
              'attr' => [
                'accept' => 'image/*', 
                'enctype' => 'multipart/form-data',
              ],
          ])
          ->add('submit', SubmitType::class, [
              'label' => 'Submit', 
          ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

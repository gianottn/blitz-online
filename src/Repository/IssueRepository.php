<?php

namespace App\Repository;

use App\Entity\Issue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Issue|null find($id, $lockMode = null, $lockVersion = null)
 * @method Issue|null findOneBy(array $criteria, array $orderBy = null)
 * @method Issue[]    findAll()
 * @method Issue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IssueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Issue::class);
    }

    public function findIssue(string $year, int $issue) {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT i
            FROM App\Entity\Issue i
            WHERE i.number = :issue
            AND i.year = :year'
        )->setParameter('issue', $issue)
        ->setParameter('year', $year);

        return $query->getResult();
    }

    public function findIssuesByYear(string $year) {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT i
            FROM App\Entity\Issue i
            WHERE i.year = :year'
        )->setParameter('year', $year);

        return $query->getResult();
    }

    // /**
    //  * @return Issue[] Returns an array of Issue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Issue
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\IssueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class MainController extends AbstractController
{
    public function __construct(ArticleRepository $ar, IssueRepository $ir)
    {
        $this->ar = $ar;
        $this->ir = $ir;
        $this->serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
    }

    /**
     * @Route("/", name="main")
     */
    public function index(): Response
    {
        $years = $this->getYears();
        $issues = $this->getIssues($years[0]);
        $issueOb = $issues[0];
        $articles = $this->getArticles($issueOb->getId());

        return $this->render('main/index.html.twig', [
            'years' => json_encode($years),
            'year' => $years[0],
            'issues' => $this->serializer->serialize($issues, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'issue' => $this->serializer->serialize($issueOb, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'articles' => $this->serializer->serialize($articles, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['issue']]),
            'article' => $this->serializer->serialize($articles[0], 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['issue']]),
        ]);
    }
    
    /**
     * @Route("/articles/{year}", name="year")
     */
    public function year(ArticleRepository $ar, IssueRepository $ir, int $year): Response
    {
        $years = $this->getYears();
        $issues = $this->getIssues($year);

        return $this->render('main/index.html.twig', [
            'years' => json_encode($years),
            'year' => $year,
            'issues' => $this->serializer->serialize($issues, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'issue' => 'none',
            'articles' => 'none',
            'article' => 'none',
        ]);
    }
 
    /**
     * @Route("/articles/{year}/{issue}", name="issue")
     */
    public function issue(ArticleRepository $ar, IssueRepository $ir, int $year, int $issue): Response
    {
        $years = $this->getYears();
        $issues = $this->getIssues($year);
        $articles = $this->getArticles($issue);

        $issueOb = $this->ir->findOneBy(['id' => $issue]);

        return $this->render('main/index.html.twig', [
            'years' => json_encode($years),
            'year' => $year,
            'issues' => $this->serializer->serialize($issues, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'issue' => $this->serializer->serialize($issueOb, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'articles' => $this->serializer->serialize($articles, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['issue']]),
            'article' => 'none',
        ]);
    }
 
    /**
     * @Route("/articles/{year}/{issue}/{article}", name="article")
     */
    public function article(ArticleRepository $ar, IssueRepository $ir, int $year, int $issue, int $article): Response
    {
        $years = $this->getYears();
        $issues = $this->getIssues($year);
        $articles = $this->getArticles($issue);

        $issueOb = $this->ir->findOneBy(['id' => $issue]);
        $articleOb = $this->ar->findOneBy(['id' => $article]);

        return $this->render('main/index.html.twig', [
            'years' => json_encode($years),
            'year' => $year,
            'issues' => $this->serializer->serialize($issues, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'issue' => $this->serializer->serialize($issueOb, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['articles']]),
            'articles' => $this->serializer->serialize($articles, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['issue']]),
            'article' => $this->serializer->serialize($articleOb, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['issue']]),
        ]);
    }

    public function getYears(): array
    {
        $years = array();
        $issues = $this->ir->findAll();
        foreach($issues as $issue) {
            if(!in_array($issue->getYear(), $years)) {
                array_push($years, $issue->getYear());
            }
        }

        rsort($years);

        return $years;
    }

    public function getIssues(int $year): array
    {
        return $this->ir->findIssuesByYear($year);
    }

    public function getArticles(int $issue): array
    {
        return $this->ir->findOneBy(['id' => $issue])->getArticles()->getValues();
    }
}
